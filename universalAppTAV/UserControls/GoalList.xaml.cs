﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using universalAppTAV.Entities;
using universalAppTAV.Services;
using universalAppTAV.Utils;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d'élément Contrôle utilisateur, consultez la page https://go.microsoft.com/fwlink/?LinkId=234236

namespace universalAppTAV.UserControls
{
    public partial class GoalList : UserControl
    {

        private Person myPerson;

        public GoalList(Person person)
        {
            this.InitializeComponent();
            this.goalPersonName.Text = person.Name;
            this.goalPersonIcon.ImageSource = ImageUtils.GetImage(person.Logo_path);
            this.Init(person);
        }

        public Person GetPerson()
        {
            return myPerson;
        }

        private async void Init(Person person)
        {
            myPerson = person;
            if (person.Current)
            {
                this.arrowUp.Visibility = Visibility.Visible;
                this.arrowDown.Visibility = Visibility.Visible;
            }
            SettingsService settingsService = new SettingsService();
            Settings settings = await settingsService.GetCurrentUserSettings();
            this.goalPersonCapital.Text = StringUtils.ToKMB(person.Capital, settings.Currency);
        }
    }
}
