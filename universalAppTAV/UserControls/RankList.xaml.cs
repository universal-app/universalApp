﻿using System;
using universalAppTAV.Entities;
using universalAppTAV.Services;
using universalAppTAV.Utils;
using Windows.UI.Xaml.Controls;
using Windows.Media.SpeechRecognition;
using Windows.Devices.Input;

// Pour en savoir plus sur le modèle d'élément Contrôle utilisateur, consultez la page https://go.microsoft.com/fwlink/?LinkId=234236

namespace universalAppTAV.UserControls
{
    public partial class RankList : UserControl
    {
        private Person myPerson;

        public RankList(Int32 rank, Person person)
        {

            this.InitializeComponent();
            this.rankOfUser.Text = rank.ToString();
            this.rankUserName.Text = person.Name;
            this.rankUserIcon.ImageSource = ImageUtils.GetImage(person.Logo_path);
            this.Init(person);
        }

        public Person GetPerson()
        {
            return myPerson;
        }

        private async void Init(Person person)
        {
            myPerson = person;
            SettingsService settingsService = new SettingsService();
            Settings settings = await settingsService.GetCurrentUserSettings();
            this.rankUserCapital.Text = StringUtils.ToKMB(person.Capital, settings.Currency);
        }
    }
}
