﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using universalAppTAV.Views;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d'élément Contrôle utilisateur, consultez la page https://go.microsoft.com/fwlink/?LinkId=234236

namespace universalAppTAV.UserControls
{
    public sealed partial class Footer : UserControl
    {
        public Footer()
        {
            this.InitializeComponent();
        }

        public void setActive(int index)
        {
            if (index.Equals(0))
            {
                iconProfile.Source = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "/Assets/man-user-active.png", UriKind.Absolute));
            }
            else if (index.Equals(1))
            {
                iconGoals.Source = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "/Assets/favourites-active.png", UriKind.Absolute));
            }
            else if (index.Equals(2))
            {
                iconRank.Source = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "/Assets/trophy-active.png", UriKind.Absolute));
            }
        }

        private void goToProfile(object sender, RoutedEventArgs e)
        {
            (Window.Current.Content as Frame).Navigate(typeof(UserProfileView));
        }

        private void goToGoals(object sender, RoutedEventArgs e)
        {
            (Window.Current.Content as Frame).Navigate(typeof(FollowersView));
        }

        private void goToRank(object sender, RoutedEventArgs e)
        {
            (Window.Current.Content as Frame).Navigate(typeof(RankView));
        }
    }
}
