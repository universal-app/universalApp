﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using universalAppTAV.Entities;
using universalAppTAV.Enums;

namespace universalAppTAV.Services
{
    public class UserService
    {
        public async Task<User> GetCurrentUser()
        {
            WebServiceManager<User> webManager = new WebServiceManager<User>(DataConnectionResourceEnum.LOCALAPI);
            return await webManager.Get();
        }

        public async Task<List<Person>> GetFollowers()
        {
            WebServiceManager<Person> webManager = new WebServiceManager<Person>(DataConnectionResourceEnum.LOCALAPI);
            return await webManager.GetAll("user/followers");
        }

        public async Task<List<User>> GetAll()
        {
            WebServiceManager<User> webManager = new WebServiceManager<User>(DataConnectionResourceEnum.LOCALAPI);
            return await webManager.GetAll("users");
        }
    }
}
