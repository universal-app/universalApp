﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using universalAppTAV.Enums;
using universalAppTAV.Utils;

namespace universalAppTAV.Services
{
    public class WebServiceManager<T> where T : class
    {
        public static String REQUEST_TYPE = "application/json";
        public static String SEPARATOR = "/";
        public static String S_SEPARATOR = "s/";

        public String DataConnectionResource { get; set; }

        public WebServiceManager(DataConnectionResourceEnum resource) {
            DataConnectionResource = EnumString.GetStringValue(resource);
        }

        public async Task<T> GetById(Int32 id) {
            T item = default(T);
            String url = typeof(T).Name + SEPARATOR + id + SEPARATOR;
            item = await HttpClientCaller<T>(url, item); return item;
        }

        public async Task<T> Get()
        {
            T item = default(T); String url = typeof(T).Name;
            item = await HttpClientCaller<T>(url, item); return item;
        }

        public async Task<List<T>> GetAll(String url) {
            List<T> item = default(List<T>);
            item = await HttpClientCaller<List<T>>(url, item);
            return item;
        }

        public async Task<T> Post(T item, String url)
        {
            T result = default(T);
            url += SEPARATOR;
            result = await HttpClientSender<T>(url, item, result);

            return result;
        }

        public async Task<List<T>> Post(List<T> items)
        {
            List<T> result = default(List<T>);
            String url = typeof(T).Name + S_SEPARATOR;
            result = await HttpClientSender<List<T>>(url, items, result);

            return result;
        }

        public async Task<T> Put(T item)
        {
            T result = default(T);
            String url = typeof(T).Name + SEPARATOR;
            result = await HttpClientPuter<T>(url, item, result);

            return result;
        }

        public async Task<List<T>> Put(List<T> items)
        {
            List<T> result = default(List<T>);
            String url = typeof(T).Name + S_SEPARATOR;
            result = await HttpClientPuter<List<T>>(url, items, result);

            return result;
        }

        public async Task<Int32> Delete(T item)
        {
            Int32 result = default(Int32);
            String url = typeof(T).Name + SEPARATOR;
            result = await HttpClientDeleter<T, Int32>(url, item, result);

            return result;
        }

        public async Task<Int32> Delete(List<T> items)
        {
            Int32 result = default(Int32);
            String url = typeof(T).Name + S_SEPARATOR;
            result = await HttpClientDeleter<List<T>, Int32>(url, items, result);

            return result;
        }

        protected async Task<TItem> HttpClientCaller<TItem>(String url, TItem item) {
            using (HttpClient client = new HttpClient()) {
                client.BaseAddress = new Uri(DataConnectionResource);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(REQUEST_TYPE));
                HttpResponseMessage response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode) {
                    String result = await response.Content.ReadAsStringAsync();
                    item = JsonConvert.DeserializeObject<TItem>(result);
                }
            }
            return item;
        }

        private async Task<TItem> HttpClientSender<TItem>(String url, TItem item, TItem result)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(DataConnectionResource);
                client.DefaultRequestHeaders
                  .Accept
                  .Add(new MediaTypeWithQualityHeaderValue(REQUEST_TYPE));
                String json = JsonConvert.SerializeObject(item);
                using (HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Post, url))
                {
                    message.Content = new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8, REQUEST_TYPE);
                    HttpResponseMessage response = await client.SendAsync(message);
                    result = await HandleResponse(result, response);
                }
            }

            return result;
        }

        private async Task<TItem> HttpClientPuter<TItem>(string url, TItem item, TItem result)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(DataConnectionResource);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(REQUEST_TYPE));
                HttpResponseMessage response = await client.PutAsync(url, new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8, REQUEST_TYPE));
                result = await HandleResponse(item, response);
            }
            return result;
        }

        private async Task<TResult> HttpClientDeleter<TItem, TResult>(string url, TItem item, TResult result) {
            using (HttpClient client = new HttpClient()) { client.BaseAddress = new Uri(DataConnectionResource);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(REQUEST_TYPE));
                using (HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Delete, url)) {
                    message.Content = new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8, REQUEST_TYPE);
                    HttpResponseMessage response = await client.SendAsync(message);
                    result = await HandleResponse(result, response);
                }
            }
            return result;
        }

        private async Task<TItem> HandleResponse<TItem>(TItem item, HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                String result = await response.Content.ReadAsStringAsync();
                JsonSerializerSettings ss = new JsonSerializerSettings();
                ss.MaxDepth = 3;
                item = JsonConvert.DeserializeObject<TItem>(result,ss);
            }

            return item;
        }
    }
}
