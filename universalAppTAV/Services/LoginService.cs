﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using universalAppTAV.Entities;
using universalAppTAV.Enums;

namespace universalAppTAV.Services
{
     public class LoginService
    {

         public async Task<User>Login(User user)
        {
            WebServiceManager<User> webManager = new WebServiceManager<User>(DataConnectionResourceEnum.LOCALAPI);

             return await webManager.Post(user, "login");
        }
    }
}
