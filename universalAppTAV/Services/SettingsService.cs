﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using universalAppTAV.Entities;
using universalAppTAV.Enums;

namespace universalAppTAV.Services
{
    class SettingsService
    {
        public async Task<Settings> GetCurrentUserSettings()
        {
            WebServiceManager<Settings> webManager = new WebServiceManager<Settings>(DataConnectionResourceEnum.LOCALAPI);
            return await webManager.Get();
        }

        public async Task<Settings> UpdateCurrentUserSettings(Settings settings)
        {
            WebServiceManager<Settings> webManager = new WebServiceManager<Settings>(DataConnectionResourceEnum.LOCALAPI);

            return await webManager.Post(settings, "settings");
        }
    }
}
