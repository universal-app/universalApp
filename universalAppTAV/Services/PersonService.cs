﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using universalAppTAV.Entities;
using universalAppTAV.Enums;

namespace universalAppTAV.Services
{
    public class PersonService
    {
        public async Task<List<Person>> GetAll()
        {
            WebServiceManager<Person> webManager = new WebServiceManager<Person>(DataConnectionResourceEnum.LOCALAPI);
            return await webManager.GetAll("userapp");
        }
    }
}
