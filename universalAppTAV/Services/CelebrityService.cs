﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using universalAppTAV.Entities;
using universalAppTAV.Enums;

namespace universalAppTAV.Services
{
    public class CelebrityService
    {
        public async Task<List<Celebrity>> GetAll()
        {
            WebServiceManager<Celebrity> webManager = new WebServiceManager<Celebrity>(DataConnectionResourceEnum.LOCALAPI);
            return await webManager.GetAll("celebrities");
        }
    }
}
