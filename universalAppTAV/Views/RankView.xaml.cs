﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using universalAppTAV.Entities;
using universalAppTAV.Services;
using universalAppTAV.UserControls;
using Windows.Devices.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace universalAppTAV.Views
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class RankView : Page
    {
        List<Person> persons;

        public RankView()
        {
            this.InitializeComponent();
            Header.setPageName("Classement");
            Footer.setActive(2);
            this.Init();
        }

        private async void Init()
        {
            PersonService personService = new PersonService();
            this.persons = await personService.GetAll();
            this.persons = this.persons.OrderByDescending(p => p.Capital).ToList();

            for(int i = 0; i < this.persons.Count(); i++)
            {
                RankList rankList = new RankList(i+1, persons[i]);
                this.rankViewGrid.Items.Add(rankList);
            }
        }

        private void listView_Item_Click(object sender, ItemClickEventArgs e)
        {
            RankList rankList = e.ClickedItem as RankList;
            Dictionary<Int32, Person> dico = new Dictionary<Int32, Person>();
            dico.Add(2, rankList.GetPerson());

            (Window.Current.Content as Frame).Navigate(typeof(UserProfileView), dico);
        }
    }
}
