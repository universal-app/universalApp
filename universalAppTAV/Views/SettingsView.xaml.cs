﻿using universalAppTAV.Entities;
using universalAppTAV.Services;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace universalAppTAV.Views
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class SettingsView : Page
    {
        SettingsService settingsService;
        Settings settings;

        public SettingsView()
        {
            this.Init();
        }

        private async void Init()
        {
            this.settingsService = new SettingsService();
            this.settings = await this.settingsService.GetCurrentUserSettings();

            this.InitializeComponent();

            foreach (ComboBoxItem item in this.currencyCbB.Items){
               if (item.Content.ToString().Equals(this.settings.Currency)){
                    this.currencyCbB.SelectedIndex = this.currencyCbB.Items.IndexOf(item);
                }   
            }

            this.notifsChB.IsChecked = this.settings.Notification;

            this.currencyCbB.SelectionChanged += CurrencyCbB_SelectionChanged;
            this.notifsChB.Click += NotifsChB_Click;
        }

        private async void NotifsChB_Click(object sender, RoutedEventArgs e)
        {
            this.settings.Notification = ((CheckBox) e.OriginalSource).IsChecked.Value;
            await this.settingsService.UpdateCurrentUserSettings(this.settings);
        }

        private async void CurrencyCbB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                this.settings.Currency = ((ComboBoxItem) e.AddedItems[0]).Content.ToString();
                await this.settingsService.UpdateCurrentUserSettings(this.settings);
            }
        }
    }
}
