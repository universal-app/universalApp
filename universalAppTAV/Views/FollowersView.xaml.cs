﻿using System;
using System.Collections.Generic;
using universalAppTAV.Entities;
using universalAppTAV.Services;
using universalAppTAV.UserControls;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace universalAppTAV.Views
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class FollowersView : Page
    {
        List<Person> persons;

        public FollowersView()
        {
            this.InitializeComponent();
            Header.setPageName("Objectifs");
            Footer.setActive(1);
            this.Init();
        }

        private async void Init()
        {
            UserService userService = new UserService();
            this.persons = await userService.GetFollowers();
            foreach (var person in persons)
            {
                GoalList goalList = new GoalList(person);
                this.followersViewGrid.Items.Add(goalList);
            }
        }

        private void listView_Item_Click(object sender, ItemClickEventArgs e)
        {
            GoalList followerList = e.ClickedItem as GoalList;
            Dictionary<Int32, Person> dico = new Dictionary<Int32, Person>();

            dico.Add(1, followerList.GetPerson());

            (Window.Current.Content as Frame).Navigate(typeof(UserProfileView), dico);
        }
    }
}
