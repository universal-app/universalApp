﻿using System.Collections.Generic;
using universalAppTAV.Entities;
using universalAppTAV.Services;
using universalAppTAV.Utils;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace universalAppTAV.Views
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class UserProfileView : Page
    {
        Person user;

        public UserProfileView()
        {
            this.InitializeComponent();
            followBtn.Visibility = Visibility.Collapsed;
        }

        private async void Init()
        {
            UserService userService = new UserService();
            this.user = await userService.GetCurrentUser();
            this.displayUser(this.user);
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            (Window.Current.Content as Frame).Navigate(typeof(FollowersView));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Dictionary<int, Person> dico = e.Parameter as Dictionary<int, Person>;
 
            if (null != dico)
            {
                Header.setPageName("Détails");
                followBtn.Visibility = Visibility.Visible;
                Person person = null;

                // From ranking view
                if (dico.ContainsKey(2))
                {
                    person = dico[2];
                    Footer.setActive(2);
                }
                else // From folowers view
                {
                    person = dico[1];
                    Footer.setActive(1);

                    if (person.Current == true)
                    {
                        followBtn.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        followBtn.Content = "NE PLUS SUIVRE";
                    }
                }

                this.displayUser(person);
            }
            else
            {
                Header.setPageName("Profil");
                Footer.setActive(0);
                this.Init();
            }
        }

        private async void displayUser(Person person)
        {
            userLogo.ImageSource = ImageUtils.GetImage(person.Logo_path);
            userNameTxtBloc.Text = person.Name;
            userJobTxtBloc.Text = person.Job;
            userAgeTxtBloc.Text = person.Age.ToString();

            SettingsService settingsService = new SettingsService();
            Settings settings = await settingsService.GetCurrentUserSettings();
            userCapitalTxtBloc.Text = StringUtils.FormatDevise(person.Capital, settings.Currency);
        }
    }
}
