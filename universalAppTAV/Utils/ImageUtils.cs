﻿using System;
using Windows.UI.Xaml.Media.Imaging;

namespace universalAppTAV.Utils
{
    public static class ImageUtils
    {
        public static BitmapImage GetImage(String uriString)
        {
            String apiImageUri = "http://localhost:8080/api/logo/";
            BitmapImage bitmap = new BitmapImage();
            bitmap.UriSource = new Uri(apiImageUri + uriString);

            return bitmap;
        }
    }
}
