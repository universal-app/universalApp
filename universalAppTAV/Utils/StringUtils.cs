﻿using System;
using System.Globalization;

namespace universalAppTAV.Utils
{
    public static class StringUtils
    {
        public static String FormatDevise(Double num, String deviseSymbol)
        {
            var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            nfi.NumberGroupSeparator = " ";
            return num.ToString("#,0", nfi) + " "+deviseSymbol;
        }

        public static string ToKMB(Double num, String deviseSymbol)
        {
            if (num > 999999999 || num < -999999999)
            {
                return num.ToString("0,,,.###B", CultureInfo.InvariantCulture) + " " + deviseSymbol;
            }
            else
            if (num > 999999 || num < -999999)
            {
                return num.ToString("0,,.##M", CultureInfo.InvariantCulture) + " " + deviseSymbol;
            }
            else
            if (num > 999 || num < -999)
            {
                return num.ToString("0,.#K", CultureInfo.InvariantCulture) + " " + deviseSymbol;
            }
            else
            {
                return num.ToString(CultureInfo.InvariantCulture) + " " + deviseSymbol;
            }
        }
    }
}
