﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace universalAppTAV.Entities
{
    public class User: Person
    {
        #region Attributs
        private String login;
        private String password;
        private Estimator estimator;
        private List<User> users;
        private List<Celebrity> celebrities;

        #endregion

        #region Properties
        public string Login { get => login; set => login = value; }
        public List<User> Users { get => users; set => users = value; }
        public List<Celebrity> Celebrities { get => celebrities; set => celebrities = value; }
        public string Password { get => password; set => password = value; }
        public Estimator Estimator { get => estimator; set => estimator = value; }
        #endregion

        #region Constructor
        public User()
        {
        }
        #endregion
    }
}