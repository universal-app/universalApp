﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace universalAppTAV.Entities
{
    public class Settings: BaseEntity
    {
        #region Attributes
        private String currency;
        private bool notification;

        public Settings()
        {
        }

        #endregion
        public string Currency { get => currency; set => currency = value; }
        public bool Notification { get => notification; set => notification = value; }
        #region Properties
        #endregion
    }
}
