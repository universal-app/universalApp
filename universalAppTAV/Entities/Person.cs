﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace universalAppTAV.Entities
{
    public class Person: BaseEntity
    {
        #region Attributs
        private String name;
        private int age;
        private String job;
        private Double capital;
        private String logo_path;
        private bool current;

        #endregion

        #region Properties
        public string Name { get => name; set => name = value; }
        public string Job { get => job; set => job = value; }
        public double Capital { get => capital; set => capital = value; }

        public string Logo_path { get => logo_path; set => logo_path = value; }
        public int Age { get => age; set => age = value; }
        public bool Current { get => current; set => current = value; }
        #endregion

        #region Constructor
        public Person()
        {

        }

        #endregion
    }
}
