﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace universalAppTAV.Entities
{
    public class Celebrity: Person
    {
        #region Attributs
        private String description;

        #endregion

        #region Properties
        public string Description { get => description; set => description = value; }
        #endregion

        #region Constructor
        public Celebrity()
        {

        }

        #endregion
    }
}