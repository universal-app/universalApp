﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace universalAppTAV.Entities
{
    public class Estimator: BaseEntity
    {
        #region Attributs
        private String login;
        #endregion

        #region properties
        public string Login { get => login; set => login = value; }
        #endregion
    }
}
