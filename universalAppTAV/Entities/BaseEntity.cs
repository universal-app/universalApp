﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace universalAppTAV.Entities
{
    public class BaseEntity
    {
        #region Attributs
        private int id;
        #endregion

        #region properties
        public int Id { get => id; set => id = value; }
        #endregion
    }
}
