﻿using Microsoft.HockeyApp;
using System;
using universalAppTAV.Entities;
using universalAppTAV.Services;
using universalAppTAV.Views;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace universalAppTAV
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public User user = new User();
        public MainPage()
        {
			this.InitializeComponent();
            HockeyClient.Current.TrackPageView("LoginPage");
        }

		private async void Btn_Click(object sender, RoutedEventArgs e)
		{
            this.user.Login = login.Text;
            this.user.Password = password.Password;

            LoginService service = new LoginService();

            if (await service.Login(user) != null) {
              (Window.Current.Content as Frame).Navigate(typeof(UserProfileView));
             HockeyClient.Current.TrackEvent("login");
             }
        }
	}
}
